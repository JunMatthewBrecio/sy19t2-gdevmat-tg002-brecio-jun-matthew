void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  stars = new Mover[100];
  value(stars);
}

Mover blackhole;
Mover stars[];

void value(Mover stars[])
{
  for(int count = 0; count < 100; count++)
  {
    float horizontalRange = random(Window.left, Window.right);
    float verticalRange = random(Window.bottom, Window.top);
    
    float gauss = randomGaussian();
    float std = 1000;
    float x = std * gauss + horizontalRange;
    float y = std * gauss + verticalRange;
    stars[count] = new Mover(x, y, random(50));
    
    stars[count].r = map(noise(random(255)), 0, 1, 0, 255);
    stars[count].g = map(noise(random(255)), 0, 1, 0, 255);
    stars[count].b = map(noise(random(255)), 0, 1, 0, 255);
  }
  
  blackhole = new Mover(random(Window.left, Window.right), random(Window.bottom, Window.top), 50);
}


void draw()
{
  background(0);
  
  for(int count = 0; count < 100; count++)
  {
    stars[count].render();
    PVector range = PVector.sub(blackhole.position, stars[count].position);
    
    range.normalize().mult(5);
    
    stars[count].position.add(range);
  }
  
  blackhole.render();
  
  if(frameCount >= 500)
  {
    frameCount = -1;
    setup();
  }
}
