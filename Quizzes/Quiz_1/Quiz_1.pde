void setup() // initialization
{
  size(1920, 1080, P3D); //P3D used to make things 3d
  background(255);
  camera(0,0,-(height/2) / tan(PI * 30 /180), // camera position
  0,0,0, // eye position
  0,-1,0); // up vector
  
}

void draw()
{
  float mean = 0;
  float std = 1000;
  float scaleStd = 100;
  float gauss = randomGaussian();
  float scaleGauss = randomGaussian();
  
  float x = std * gauss + mean;
  float y = random(-1000, 1000);
  float s = scaleStd * scaleGauss + mean;
  
  float r = random(255);
  float g = random(255);
  float b = random(255);
  float o = random(10, 50);
  
  noStroke();
  fill(r, g, b, o);
  circle(x,y,s);
}
