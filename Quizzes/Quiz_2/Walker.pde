class Walker
{
  float xTime = 0;
  float yTime = 1;
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  float radius = 1;
  
  void perlinNoise()
  {
    float xValue = noise(xTime);
    float xPosition = map(xValue, 0, 1, 0, width);
    float yValue = noise(yTime);
    float yPosition = map(yValue, 0, 1, 0, height);
    
    float radiusValue = noise(radius);
    float radiusPosition = map(radiusValue, 0, 1, 0, 100);
    radius += 0.01;
    
    circle(xPosition, yPosition, radiusPosition);
    
    fill(map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255), 255); 
    r_t += 0.1f;
    g_t += 0.2f;
    b_t += 0.3f;
    
    xTime += 0.01;
    yTime += 0.01;
  }  
}

// Source: https://natureofcode.com/book/introduction/?fbclid=IwAR33LIY3zT56hdrWIjALfFDV3Bmi9FzlaxAw_oYjMrfU0SGrw0MNT_kSsiQ
