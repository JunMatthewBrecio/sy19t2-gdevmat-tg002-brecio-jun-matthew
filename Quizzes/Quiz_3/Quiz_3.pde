Mover mover;
Mover circles[];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  //mover = new Mover();
  //mover.position.x = Window.left + 50;
  
  
  circles = new Mover[10];
  
  for(int count = 0; count < 10; count++)
  {
    circles[count] = new Mover();
    circles[count].position.x = Window.left + 50;
    circles[count].setColor(random(255), random(255), random(255), random(255));
    circles[count].setMover(10-count, 100-(count*10));
  }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1);

void draw()
{
  background(255);
  for(int count = 0; count < 10; count++)
  {
    circles[count].update();
    circles[count].render();
    
    circles[count].applyForce(wind);
    circles[count].applyForce(gravity);
    
    if(circles[count].position.y < Window.bottom)
      {
        circles[count].velocity.y *= -1;
        circles[count].position.y = Window.bottom;
      }
     if(circles[count].position.x > Window.right)
      {
        circles[count].velocity.x *= -1;
        circles[count].position.x = Window.right;
      }
  }
}
