public class Mover
{
   public PVector position = new PVector();
   public PVector velocity = new PVector();
   public PVector acceleration = new PVector();
   
   public float mass;
   public float scale;
   public float r = 255, g = 255, b = 255, a = 255;
   Mover()
   {
   }
   
   Mover(float x, float y)
   {
     position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Mover(PVector position)
   {
      this.position = position; 
   }
   
   Mover(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
   
   
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, scale); 
   }
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
   
   public void setMover(float mass, float scale)
   {
     this.mass = mass;
     this.scale = scale;
   }
   
   public void update()
   {
       this.velocity.add(this.acceleration);
       this.velocity.limit(30);
       this.position.add(this.velocity);
       
       this.acceleration.mult(0);
   }
   
   public void applyForce(PVector force)
  {
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
}
