void setup() // initialization
{
  size(1920, 1080, P3D); //P3D used to make things 3d
  camera(0,0,-(height/2) / tan(PI * 30 /180), // camera position
  0,0,0, // eye position
  0,-1,0); // up vector
  
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x,y);
}

void draw()
{
  background(1);
  
  Vector2 mouse = mousePos();
  Vector2 handle = mousePos();
  
  mouse.normalize();
  mouse.mult(500);
  
  handle.normalize();
  handle.mult(100);
  
  strokeWeight(15);
  stroke(255, 0, 0);
  
  line(0,0,mouse.x,mouse.y);
  
  
  stroke (250);
  line(0,0,handle.x,handle.y);
  
}
