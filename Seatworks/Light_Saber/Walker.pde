class Walker
{
  //float xPosition;
  //float yPosition;
  
  Vector2 position = new Vector2();
  
  void render()
  {
    circle(position.x, position.y, 30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if (decision == 0)
    {
      position.y++;
    }
    else if(decision == 1)
    {
      position.y--;
    }
    else if(decision == 2)
    {
      position.x++;
    }
    else if(decision == 3)
    {
      position.x--;
    }
    else if(decision == 4)
    {
      position.y++;
      position.x++;
    }
    else if(decision == 5)
    {
      position.y--;
      position.x--;
    }
    else if(decision == 6)
    {
      position.y--;
      position.x++;
    }
    else if(decision == 7)
    {
      position.y++;
      position.x--;
    }
  }
}
