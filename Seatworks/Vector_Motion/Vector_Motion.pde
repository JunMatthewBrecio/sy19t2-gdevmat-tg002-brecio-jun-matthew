Mover mover;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left+50;
  mover.acceleration = new PVector(0.1, 0);
  mover.deceleration = new PVector(-0.1, 0);
}

void draw()
{
  background(255);
 
  mover.render();
  mover.update();
  mover.setColor(100, 255, 50, 255);
}
