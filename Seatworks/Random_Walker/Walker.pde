class Walker
{
  float xPosition;
  float yPosition;
  
  void render()
  {
    circle(xPosition, yPosition, 30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if (decision == 0)
    {
      yPosition++;
    }
    else if(decision == 1)
    {
      yPosition--;
    }
    else if(decision == 2)
    {
      xPosition++;
    }
    else if(decision == 3)
    {
      xPosition--;
    }
    else if(decision == 4)
    {
      yPosition++;
      xPosition++;
    }
    else if(decision == 5)
    {
      yPosition--;
      xPosition--;
    }
    else if(decision == 6)
    {
      yPosition--;
      xPosition++;
    }
    else if(decision == 7)
    {
      yPosition++;
      xPosition--;
    }
  }
}
